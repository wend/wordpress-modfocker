# wordpress-modfocker

Wordpress mo**D**F**ocker**: HTTPS, NFS, Caching e Sendmail

O objetivo do projeto é criar colaborativamente uma imagem docker para o Wordpress, contemplando uma serie de recursos que normalmente agente ve a galera sofrendo para chegar nesse objetivo.

Esse projeto será aberto, free, open e disponivel no dockerhub.

TODO
* [ ] Dockerfile
* [ ] docker-compose.yml
* [ ] Documentação de uso para disponibilizar no dockerhub
* [ ] ...

Para facilitar o entendimento da idéia, segue diagrama abaixo:


```plantuml
@startuml
title 
  <size:20><b>Wordpress moD</b>F<b>ocker: HTTPS, NFS e Caching</b></size>
  <size:12><i>por Walker de Alencar</i> | powered by PlantUML</size>

end title
skinparam monochrome true

node "Wordpress" {
    frame "Webserver" {
        [apache]
        [nginx]
    }
    frame "Content"  {
        folder "/srv/wordpress/" {
            component ["wp-content"] as wpcontent
        }
    }
}
node "Database"{
    database "MariaDB" {
        [wp_site]
    }
}
node "NFS" {
    folder "/var/nfs/"{
        component [general] as nfs
    }
}
node "Security" {
    [LetsEncript]
}
node "Caching" {
    [Varnish]
}

Caching <--> Webserver
Webserver <--> Content
Wordpress --> MariaDB
Content -left-> nfs
Wordpress <-> LetsEncript
@enduml
```

## Referências
* https://hub.docker.com/_/wordpress
* https://docs.docker.com/compose/wordpress/
* https://hub.docker.com/r/hochzehn/wordpress-sendmail/
* https://github.com/hochzehn/wordpress-sendmail-docker/blob/master/Dockerfile
* https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-18-04
* https://www.lambda3.com.br/2017/02/migrando-um-blog-do-wordpress-para-um-conteiner-docker/